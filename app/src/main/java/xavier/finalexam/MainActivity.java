package xavier.finalexam;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Scanner;

import xavier.finalexam.db.SongsDateSource;
import xavier.finalexam.model.Song;

public class MainActivity extends Activity {

    Button btnLoadSong;
    Button btnGetSong;
    EditText edtSongPos;
    TextView txtResults;
    boolean dbLoad = false;
    Context context = MainActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupViews();
        setupListeners();
    }



    public void setupViews(){

        btnLoadSong = findViewById(R.id.btn_get_song);
        btnGetSong = findViewById(R.id.btn_load_song_file);
        edtSongPos = findViewById(R.id.edt_song_pos);
        txtResults = findViewById(R.id.txt_display_result);
    }

    private void setupListeners() {
        btnGetSong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFromWeb();
            }
        });
        btnLoadSong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFromDB();
            }
        });

    }

    private void loadFromDB() {
        if(dbLoad){
            if(!edtSongPos.getText().toString().isEmpty()){
                    displaySong();
            }else{
                txtResults.setText("Please Enter a song Position");
            }

        } else{
            txtResults.setText("Please Load Song File");

        }

    }

    private void displaySong() {
        SongsDateSource songsDB = new SongsDateSource(this);
        ArrayList<Song> songs;

        songs = songsDB.getSongsByPos(edtSongPos.getText().toString());
        if(songs.size() != 0){
            for(int i = 0; i<songs.size()-1;i++){
                txtResults.setText(songs.get(i).toString());
            }

        } else{
            txtResults.setText("Not Found");

        }

    }

    private void loadFromWeb() {
        //SongsDateSource songsDB = new SongsDateSource(context);
        //songsDB.deleteAll();
        MyTask task = new MyTask();
        task.execute("http://bonenfan.dev.fast.sheridanc.on.ca/songs.json");
        dbLoad = true;

    }

    public class MyTask extends AsyncTask<String,Void,String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                URL url = new URL(strings[0]);
                URLConnection conn = url.openConnection();
                HttpURLConnection httpConn = (HttpURLConnection) conn;

                int responseCode = httpConn.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {

                    InputStream Is = httpConn.getInputStream();
                    Scanner scanner = new Scanner(Is);
                    StringBuilder builder = new StringBuilder();

                    while (scanner.hasNextLine()) {
                        builder.append(scanner.nextLine());
                    }
                    return songInfo(builder.toString());

                } else {
                    return "HTTP Error";
                }

            } catch (Exception e) {
                return e.toString();
            }
        }

        //Parse JSON object for sufficient output
        private String songInfo(String s) throws JSONException {
            StringBuilder builder = new StringBuilder();
            SongsDateSource songsDB = new SongsDateSource(getApplicationContext());
            //Local Variable for String Builder - Final Output
                //Create JSONObject for Inputed String
            JSONObject jWrapper = new JSONObject(s);
            JSONArray songsArray = jWrapper.getJSONArray("songs");
            for (int i = 0; i < songsArray.length(); i++) {
                JSONObject songDet = songsArray.getJSONObject(i);
                String pos = songDet.getString("position");
                String name = songDet.getString("name");
                String length = songDet.getString("length");
                //builder.append(pos).append(name).append(length).append("\n");
                songsDB.saveSong(new Song(pos,name,length));
            }
            return builder.toString();
        }

        @Override
        protected void onPostExecute(String s) {

            txtResults.setText(s);

        }
    }
}
