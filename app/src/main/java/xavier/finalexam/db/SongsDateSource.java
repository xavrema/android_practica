package xavier.finalexam.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import xavier.finalexam.model.Song;

/**
 * Created by Xavie on 2018-01-10.
 */

public class SongsDateSource {


    //Access DataBase
    private SQLiteDatabase database;
    private SQLiteOpenHelper openHelper;

    private static final String DB_NAME = "SongsDB.db";
    private static final int DB_VERSION = 1;


    private static final String SONGS_TABLE = "Songs";

    private static final String ID = "_id";
    private static final int ID_COLUMN = 0;

    private static final String POSITION = "position";
    private static final int POSITION_COLUMN = 1;

    private static final String NAME = "name";
    private static final int NAME_COLUMN = 2;

    private static final String LENGTH = "length";
    private static final int LENGTH_COLUMN = 3;

    private static final String CREATE_SONGS_TABLE =
            "CREATE TABLE " + SONGS_TABLE + " (" +
                    ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    POSITION + " TEXT, " +
                    NAME + " TEXT, " +
                    LENGTH + " TEXT)";

    public SongsDateSource(Context context) {

        openHelper = new DBHelper(context, DB_NAME, DB_VERSION);

    }



    public Song saveSong(Song song) {

        //First time that this is called
        //Give you a handle to Database if it ``
        //-- Will create DataBase
        database = openHelper.getWritableDatabase();


        //.put(NAME OF COLUMN, Value you are saving)
        ContentValues cv = new ContentValues();
        cv.put(POSITION, song.getPosition());
        cv.put(NAME, song.getName());
        cv.put(LENGTH, song.getLength());

        long id = database.insert(SONGS_TABLE, null, cv);

        song.setDbId(id);

        database.close();

        return song;

    }

    //Helps create DataBase
    private static class DBHelper extends SQLiteOpenHelper {


        public DBHelper(Context context, String name, int version) {
            super(context, name, null, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_SONGS_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + SONGS_TABLE);
            onCreate(db);
        }
    }

    public ArrayList<Song> getSongsByPos(String position){

        ArrayList<Song> songs = new ArrayList<>();

        database = openHelper.getReadableDatabase();

        //Where clause
        String selection = "position=?";

        //Where clause is name
        //String array because this is
        String[] selectionArgs = new String[]{position};

        Cursor result =
                database.query(SONGS_TABLE, null, selection, selectionArgs, null,null,null);

        while (result.moveToNext()){
            long dbId = result.getLong(ID_COLUMN);
            String pos = result.getString(POSITION_COLUMN);
            String name = result.getString(NAME_COLUMN);
            String length = result.getString(LENGTH_COLUMN);

            songs.add(new Song(pos, name, length, dbId));

        }
        result.close();
        database.close();

        return songs;
    }
    public void deleteAll() {

        database = openHelper.getWritableDatabase();
        database.delete(SONGS_TABLE, null, null);
        database.close();
    }



}
