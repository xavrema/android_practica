package xavier.finalexam.model;

/**
 * Created by Xavie on 2018-01-10.
 */

public class Song{

    private String position;
    private String name;
    private String length;
    private long dbId;

    public Song(String position, String name, String length, long dbID){
        this.position = position;
        this.name = name;
        this.length = length;
        this.dbId = dbID;
    }

    public Song(String position, String name, String length){
        this.position = position;
        this.name = name;
        this.length = length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setDbId(long dbId) {
        this.dbId = dbId;
    }

    public String getPosition() {
        return position;
    }

    public String getLength() {
        return length;
    }

    public String getName() {
        return name;
    }

    public long getDbId() {
        return dbId;
    }

    @Override
    public String toString() {
        return"Song is " + getName();
    }
}
